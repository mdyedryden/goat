from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox(
            executable_path='/usr/local/bin/geckodriver'
        )

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Check out homepage
        self.browser.get('http://localhost:8000')

        # Window title test
        self.assertIn('Car', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Car', header_text)

        # Input for car
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a car item'
        )

        # User types car's brand
        inputbox.send_keys('Honda')

        # When user presses enter, page updates and lists the item
        inputbox.send_keys(Keys.Enter)
        time.sleep(1)

        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
            any(row.text == 'Honda' for row in rows)
        )

        # still a text box to enter another item
        
        # page updates again showing both items in the list


        # Other tests


if __name__ == '__main__':
    unittest.main(warnings='ignore')
