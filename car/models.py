from django.db import models


class Car(models.Model):

    make = models.CharField(max_length=10)
    model = models.CharField(max_length=10)
    year = models.IntegerField()

    class Meta:
        models.UniqueConstraint(
            fields=[['make', 'model', 'year']], name="unique_car"
        )
        models.Index(fields=['make', 'model', 'year'], name="index_together")

    def __str__(self):
        return f"{self.make} {self.model} {self.year}"
