from itertools import product
from django.core.management.base import BaseCommand
from django.db import connection
from car.models import Car
import time


class Command(BaseCommand):
    def _request_data(self, *args, **options):
        product_list = self.setup_product()

        function_start_time = time.perf_counter()

        # Check if item exists already
        model_instances = []
        models_to_create = []

        for i, item in enumerate(product_list):
            if i % 10000 == 0:
                print(
                    f"item {i}: "
                    f"{time.perf_counter() - function_start_time}"
                    " seconds elapsed from function start"
                )
                print(f"item {i} number of queries: {len(connection.queries)}")

            # Grab data, but append it to models_to_create if it doesn't exist
            try:
                model_instances.append(
                    Car.objects.get(make=item[0], model=item[1], year=item[2])
                )

            except Car.DoesNotExist:
                car_tuple = (
                    (item[0], item[1], item[2])
                )
                models_to_create.append(car_tuple)

        function_end_time = time.perf_counter()

        # # # DEBUG INFO # # #
        print(
            "Performance Time: "
            f"{function_end_time-function_start_time:0.4f} seconds"
        )
        print(f"Total number of queries: {len(connection.queries)}")
        print("-"*50)
        print(model_instances[0:10])
        print(len(model_instances))
        print("-"*50)
        print(models_to_create[0:10])
        print(len(models_to_create))
        # # # DEBUG INFO # # #

        return (model_instances, models_to_create)

    def setup_product(self):
        # valid makes: "brand0" ~ "brand199"
        make_list = [f"brand{i}" for i in range(100, 200)]

        # valid models: "model0" ~ "model499"
        model_list = [f"model{i}" for i in range(490, 510)]

        # valid models: 1800 ~ 1999
        year_list = [1800 + i for i in range(190, 220)]

        product_list = list(product(make_list, model_list, year_list))

        return product_list

    def handle(self, *args, **options):
        self._request_data()
