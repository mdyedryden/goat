from itertools import product
from django.core.management.base import BaseCommand
from car.models import Car


class Command(BaseCommand):
    def _generate_model(self, *args, **options):
        make_list = []
        model_list = []
        year_list = []

        for i in range(200):
            make = f"brand{i}"
            year = 1800 + i

            make_list.append(make)
            year_list.append(year)

        for i in range(500):
            model = f"model{i}"
            model_list.append(model)

        print("lists processed")
        product_list = list(product(make_list, model_list, year_list))
        print("product_list created")

        # insert data into the DB
        model_instances = []

        for item in product_list:
            model_instances.append(
                Car(make=item[0], model=item[1], year=item[2])
            )

        Car.objects.bulk_create(model_instances, batch_size=20)

    def handle(self, *args, **options):
        self._generate_model()
